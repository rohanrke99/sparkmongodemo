package com.mongodb.service;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.model.Address;
import com.mongodb.model.Company;
import com.mongodb.model.User;
import org.bson.Document;
import org.eclipse.jetty.http.HttpStatus;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class UserServiceImpl implements UserService {

    private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    MongoClient client = new MongoClient("localhost", 27017); //connect to mongodb
    Datastore datastore = new Morphia().createDatastore(client, "user");




    @Override
    public String addPost(User user, Address address, Company company) {

        User dbuser = getUserByEmail(user.getEmail());

        if (dbuser == null) {
            datastore.save(user, address, company);
            logger.info("User Creater Successfully");
            return "Successfully Created";

        } else {

            return "User already exist";

        }

    }

    @Override
    public List<User> getAllPost() {


        List<User> list = datastore.find(User.class).asList();

        if (list != null) {
            logger.info("Getting all users");
            return list;
        }
        logger.info("no users found");
        return null;

    }

    @Override
    public User getUserByEmail(String email) {

        logger.info("email");
        User user = datastore.find(User.class,"email",email).get();
        if (user!=null){
            return user;
        }else {
            logger.info("no user found by emailId : " + email);
        }
        return null;
    }

    @Override
    public Integer updateUser(User user, Address address, Company company) {


        User dbUser = getUserByEmail(user.getEmail());

        if (dbUser != null) {
            logger.info("Updating a user");
            user.setId(dbUser.getId());
            datastore.merge(user);
            return HttpStatus.OK_200;
        } else {
            logger.info("User not found");
            return HttpStatus.NOT_FOUND_404;
        }


    }
}
