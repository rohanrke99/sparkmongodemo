package com.mongodb.service;

import com.mongodb.model.Address;
import com.mongodb.model.Company;
import com.mongodb.model.User;

import java.util.List;

public interface UserService {

    // Creates a new user
     String addPost(User user, Address address, Company company);

    // Returns a list of all users
     List<User> getAllPost();

    //Returns a single user
     User getUserByEmail(String email);

    //Updates a specific user
     Integer updateUser(User user, Address address, Company company);
}
