package com.mongodb;

import spark.Request;
import spark.Response;
import spark.Spark;
import spark.Route;

public class HelloWorldSparkStyle {

    public static void main(String[] args) {
        Spark.get("/",new Route() {
            public Object handle(Request request, Response response) throws Exception {
                return "spark";
            }
        });


    }
}
