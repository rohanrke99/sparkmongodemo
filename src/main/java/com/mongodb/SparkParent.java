package com.mongodb;

import spark.Spark;

public class SparkParent {


    public  void stop(){
        Spark.get("/stop", (req, res) -> {
            Spark.stop();
            return "doh";
        });

    }
}
