package com.mongodb.model;

import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

import java.util.UUID;


public class Address {


    @Id
    private String id;

    private String city;

    private String street;

    private String zip;

    private String state;

    private String country;

    /**
     * Instantiates a new address.
     */
    public Address() {
        super();
    }

    /**
     * Instantiates a new address.
     *
     * @param id
     *            the id
     * @param city
     *            the city
     */
    public Address(String id, String city) {
        super();
        this.id = UUID.randomUUID().toString();
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }


}
