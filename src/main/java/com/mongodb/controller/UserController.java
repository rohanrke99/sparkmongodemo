package com.mongodb.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.mongodb.model.Address;
import com.mongodb.model.Company;
import com.mongodb.model.User;
import com.mongodb.service.UserService;
import spark.Spark;


public class UserController {


    public UserController(Integer port, UserService userService) {

        Gson gson = new Gson();

        // sets spark port
        Spark.port(port);

        // create a new user on consuming json request
        Spark.post("/users", (req, res) -> {
            res.type("application/json");
            User user = gson.fromJson(req.body(), User.class);
            Address address = gson.fromJson(req.body(), Address.class);
            Company company = gson.fromJson(req.body(), Company.class);
            return userService.addPost(user, address, company);
        }, gson::toJson);

        // gives a list of users
        Spark.get("users", (req, res) -> {

            res.type("application/json");
            return userService.getAllPost();
        }, gson::toJson);

        // gets an user by email
        Spark.get("/users/:email", (req, res) -> {

            String email = req.params(":email");
            System.out.println("id " + email);
            User user = userService.getUserByEmail(req.params(":email"));
            res.type("application/json");
            if (user != null) {
                return user;
            } else {
                return "No post found";
            }

        }, gson::toJson);

        // updates an user based on verifying email on consuming json request
        Spark.put("/users", (req, res) -> {
            res.type("application/json");
            User user = gson.fromJson(req.body(), User.class);
            Address address = gson.fromJson(req.body(), Address.class);
            Company company = gson.fromJson(req.body(), Company.class);

            Integer statusCode = userService.updateUser(user, address, company);
            res.status(statusCode);
            if (statusCode == 200) {
                res.body("Succesfully updated user");
            } else {
                res.body("User not found.");
            }
            return statusCode + ": " + res.body();

        }, gson::toJson);

    }

}
